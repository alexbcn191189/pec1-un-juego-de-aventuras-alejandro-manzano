
El juego consta de 3 partes, el juego está basado en el pokemon para tener una ilustración más bonita

Home:

En la home hay 3 botones y podrás elegir 1 entrenador y 3 pokemons

- Jugar la partida, donde te dirigirá al main
- Salir, saldrás del juego
- Idioma, donde se cambiara el idioma (Castellano, ingles, Unow)

Al seleccionar las flechas podrás escoger
- entrenador
- pokemon1, pokemon2, pokemon3


Main:

Se hará un random al inicio para ver quien comienza la maquina o el usuario

Una vez comenzado el jugador insulta, la máquina responde se comprueba el resultado y le toca a la máquina.
Cuando uno gane por 3 puntos se termina la partida y vamos a la pantalla de game over

(Pokeballs vidas restantes, (tambien esta el numero de puntos que llevas por si acaso))

Al perder una pokeball se cambia de pokémon

GameOver:

Te mostrará si has ganado o perdido. (si pierdes significa que ha ganado la máquina)

- si pierdes el pikachu llora

- si ganas el pikachu es feliz y habrá mini pikachus saltando

Se mostrará dos opciones

Volver a la home o volver a jugar





Requisitos:

Listo - Usar tres escenas de juego: menú (con botones de jugar y salir), juego y final de partida.
Listo - Implementar el bucle de juego usando scripts en C#.
Listo - Determinar quién ha ganado e indicarlo en la pantalla final.
Listo - Volver a la primera pantalla y poder jugar más de una partida sin tener que reiniciar el juego.
Listo - Definir una buena estructura de datos para guardar la información del juego.
Listo - Claridad y sencillez en el código.
Listo - Buena documentación y explicación del trabajo realizado.
Listo - Implementar el juego utilizando una máquina de estados.
Listo - Añadir elementos gráficos (background, sprites, animaciones...).
Listo - Añadir sonidos.
Listo - Leer los insultos y respuestas de un fichero de texto o JSON utilizando el directorio Resources de Unity.