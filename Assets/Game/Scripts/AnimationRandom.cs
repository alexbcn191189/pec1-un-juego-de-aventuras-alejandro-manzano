﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRandom : MonoBehaviour {
    public Animator[] pikas;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < pikas.Length; i++) {
            StartCoroutine(PlayAnimation(Random.Range(0f, 6f), pikas[i]));
        }
    }

    public IEnumerator PlayAnimation(float _time, Animator _pikas)
    {
        yield return new WaitForSeconds(_time);
        _pikas.SetBool("Go", true);
    }

}
