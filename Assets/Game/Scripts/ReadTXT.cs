﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

/*
 * Esta clase leera los archivos txt
 */ 
public class ReadTXT : MonoBehaviour {

    //Las rutas de los archivos
    private readonly string insultosEspPath = "Assets/Resources/InsultosEspanol.txt";
    private readonly string respuestasEspPath = "Assets/Resources/RespuestasEspanol.txt";
    private readonly string insultosEnPath = "Assets/Resources/InsultosIngles.txt";
    private readonly string respuestasEnPath = "Assets/Resources/RespuestasIngles.txt";

    //Este instance lo usamos para fecilitar la llamada y poder cambiar el idioma de los textos
    public static ReadTXT instance;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(this);
        } else {
            Destroy(this);
        }
    }

    // Iniciamos el inicio de las lecturas de txt
    public void Start()
    {
        DataBase.instance.insultos = new List<string>();
        DataBase.instance.respuestas = new List<string>();

        if(!PlayerPrefs.HasKey(SaveGame.idioma) || PlayerPrefs.GetInt(SaveGame.idioma) == 0) {
            ReadTxt(insultosEspPath, DataBase.instance.insultos);
            ReadTxt(respuestasEspPath, DataBase.instance.respuestas);
        } else {
            ReadTxt(insultosEnPath, DataBase.instance.insultos);
            ReadTxt(respuestasEnPath, DataBase.instance.respuestas);
        }
    }

    //Leemos los archivos txt y lo guardamos para posterior uso
    private void ReadTxt(string path, List<string> ListString)
    {
        StreamReader read = new StreamReader(path);
        string readLine = read.ReadLine();
        while (readLine != null) {
            ListString.Add(readLine);
            readLine = read.ReadLine();
        }
        read.Close();
    }
}

