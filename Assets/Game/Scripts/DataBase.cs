﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Usaremos esta clase como una database para usar sprites etc.. del juego desde los scripts 
 */

public class DataBase : MonoBehaviour
{
    public Sprite[] entrenadores;
    public Sprite[] pokemons;
    public Sprite[] background;

    public static DataBase instance;
    public List<string> insultos;
    public List<string> respuestas;

    public AudioClip[] sounds;
    public Font letraUnow, letraNormal;

    // Creamos una instance para facilitar la llamada
    void Awake()
    {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(this);
        } else {
            Destroy(this);
        }
    }
}
