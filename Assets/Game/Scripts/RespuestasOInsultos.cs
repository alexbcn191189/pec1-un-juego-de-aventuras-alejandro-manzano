﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Prefab que se controlara el texto y enviara mensaje al MainController
 */
public class RespuestasOInsultos : MonoBehaviour {

    public Text texto;
    private int id;
    private bool respuesta;
    private MainController mainController;

    private static int RESPUESTA_CORRECTA;

    //Creamos el prefab
    public void Create(string _text, bool _respuesta, int _id, MainController _mainController)
    {
        if (PlayerPrefs.GetInt(SaveGame.idioma) == 1) texto.font = DataBase.instance.letraUnow;
        texto.text = _text;
        id = _id;
        respuesta = _respuesta;
        mainController = _mainController;
    }

    #region buttons
    public void Click()
    {
        if (respuesta) {
            mainController.Control();
            mainController.Control(texto.text,id == RESPUESTA_CORRECTA );
        } else {
            RESPUESTA_CORRECTA = id;
            mainController.Control(texto.text);
        }
    }
    #endregion
}
