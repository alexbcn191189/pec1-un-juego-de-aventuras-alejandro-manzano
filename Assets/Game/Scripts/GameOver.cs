﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * Cuando se termina una partida, mostramos si ha ganado o perdido 
 */
public class GameOver : MonoBehaviour {

    //Texto titulo y botones
    public Text gameOverText, HomeText, restartText;

    //Imagen si ponemos el pika triste o el feliz
    public Sprite pikaTriste, pikaFeliz;
    public Image pikachuImagen;
    public GameObject AnimationPikas;

    void Start()
    {
        //Ponemos la interfaz si ha ganado o perdido y depende del idioma, si el proyecto fuera mas grande tendría un manager control de idiomas para facilitar el codigo
        switch (PlayerPrefs.GetInt(SaveGame.idioma)) {
            case 0:
            StartAfter("Inicio", "Jugar de nuevo", "Has Ganado", "La maquina ha ganado");
            break;
            case 1:
            gameOverText.font = HomeText.font = restartText.font = DataBase.instance.letraUnow;
            StartAfter("Inicio", "Jugar de nuevo", "Has Ganado", "La maquina ha ganado");
            break;
            case 2:
            StartAfter("Home", "Restart", "Has Ganado", "La maquina ha ganado");
            break;
        }
    }

    private void StartAfter(string _HomeText, string _restartText, string _gameOverText, string _text)
    {
        HomeText.text = _HomeText;
        restartText.text = _restartText;

        if (MainController.puntuacionPlayer >= 3) {
            pikachuImagen.sprite = pikaFeliz;
            gameOverText.text = _gameOverText;
            AnimationPikas.SetActive(true);
        } else {
            pikachuImagen.sprite = pikaTriste;
            gameOverText.text = _text;
        }
    }

    #region buttons
    public void ButtonHome() {
        SceneManager.LoadScene(0);
    }

    public void ButtorNext() {
        SceneManager.LoadScene(1);
    }
    #endregion
}

