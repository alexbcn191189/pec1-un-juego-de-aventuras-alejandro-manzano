﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainController : MonoBehaviour {

    // Images players and pokemon
    public Image playerImage, maquinaImage;
    public Image pokemonImagePlayer, pokemonImageMaquina;
    public Image[] vidasPokeballsPlayer;
    public Image[] vidasPokeballsMaquina;
    public Image fondo;

    // Chat players
    public GameObject playerTextGameObject, maquinaTextGameObject;
    public Text playerText, maquinaText;

    // Contenido de respuesta y respuestas
    public Text[] respuestasText;
    public GameObject contentRespuesta;
    public GameObject respuetasInsultoPrefab;

    private List<GameObject> respuestasInsultosInstances = new List<GameObject>();

    public Sprite[] maquinaSprites;

    public bool turnoJugador;
    //Puntuacion
    public static int puntuacionPlayer;
    public static int puntuacionMaquina;
    public Text puntuacionPlayerText, puntuacionMaquinaText;

    public AudioSource aciertaAudio, fallaAudio;

    // Esta variable representa el estado actual de la maquina 
    private int Estado;
    public enum Estados {
        EINSULTO,
        INSULTO,
        ERESPUESTA,
        RESPUESTA,
        RESULTADO,
        REPETIR,
        FINALIZADO
    };

    void Start()
    {
        //Añadimos el idioma selecionado (En este caso si han elegido el Unow)
        if (PlayerPrefs.GetInt(SaveGame.idioma) == 1) playerText.font = maquinaText.font = DataBase.instance.letraUnow;

        //Inicializamos la puntuacion
        puntuacionMaquina = puntuacionPlayer = 0;

        //Hacemos un aleatorio fondo, y personaje de la maquina
        fondo.sprite = DataBase.instance.background[Random.Range(0, DataBase.instance.background.Length)];
        int randommaquina = Random.Range(0, DataBase.instance.entrenadores.Length);

        //Buscamos la imagen del jugador para añadirle y que no se repeta con la maquina
        int randomPlayer = PlayerPrefs.GetInt(SaveGame.entrenador);
        while (randommaquina == randomPlayer) randommaquina = Random.Range(0, DataBase.instance.entrenadores.Length);

        //Ponemos las imagenes entrenadores y pomons
        maquinaImage.sprite = DataBase.instance.entrenadores[randommaquina];
        playerImage.sprite = DataBase.instance.entrenadores[PlayerPrefs.GetInt(SaveGame.entrenador)];
        pokemonImagePlayer.sprite = DataBase.instance.pokemons[PlayerPrefs.GetInt(SaveGame.pokemon1)];
        pokemonImageMaquina.sprite = DataBase.instance.pokemons[Random.Range(0, DataBase.instance.pokemons.Length)];

        //hacemos un random para elegir si comenzar jugador o maquina
        if (Random.Range(0, 2) == 1) {
            turnoJugador = false;
        } else {
            turnoJugador = true;
        }
        Control();
    }

    // Esta funcion controla la logica principal de la maquina de estados 
    public void Control(string _text = "", bool _acertado = false)
    {

        switch (Estado) {
            case (int)Estados.EINSULTO:
                Estado = (int)Estados.INSULTO;
                StartCoroutine(PonerInsultos(2));
                break;

            case (int)Estados.INSULTO:
                Estado = (int)Estados.ERESPUESTA;
                ElegirInsultoRespuesta(_text);
                Control();
                break;

            case (int)Estados.ERESPUESTA:
                Estado = (int)Estados.RESPUESTA;
                StartCoroutine(PonerRespuestaCoroutine(2));
                break;

            case (int)Estados.RESPUESTA:
                Estado = (int)Estados.RESULTADO;
                ElegirInsultoRespuesta(_text);
                break;

            case (int)Estados.RESULTADO:
                Resultado(_text, _acertado);
                if (Finalizado()) {
                    Estado = (int)Estados.FINALIZADO;
                } else {
                    Estado = (int)Estados.REPETIR;
                }
                Control();
                break;

            case (int)Estados.REPETIR:
                Estado = (int)Estados.EINSULTO;
                Control();
                break;

            case (int)Estados.FINALIZADO:
                Invoke("GameOver",1);
                break;
        }
    }

   
    //Mostramos los insultos para que se puede elegir
    public IEnumerator PonerInsultos(float _time)
    {
        for (int i = 0; i < DataBase.instance.respuestas.Count; i++) if (respuestasInsultosInstances.Count > i) respuestasInsultosInstances[i].SetActive(false);

        yield return new WaitForSeconds(_time);
        for (int i = 0; i < DataBase.instance.insultos.Count; i++) {
            if (respuestasInsultosInstances.Count <= i) {
                GameObject riObj = Instantiate(respuetasInsultoPrefab);
                riObj.GetComponent<RespuestasOInsultos>().Create(DataBase.instance.insultos[i], false, i, this);
                riObj.GetComponent<Image>().color = (Color.red);
                riObj.transform.SetParent(contentRespuesta.transform);
                respuestasInsultosInstances.Add(riObj);
            } else {
                respuestasInsultosInstances[i].GetComponent<RespuestasOInsultos>().Create(DataBase.instance.insultos[i], false, i, this);
                respuestasInsultosInstances[i].GetComponent<Image>().color = (Color.red);
                respuestasInsultosInstances[i].SetActive(true);
            }
        }
        if (!turnoJugador) JugarMaquina();

    }

    //Jugador o maquina elige un insulto
    public void ElegirInsultoRespuesta(string _text)
    {
        if (turnoJugador) {
            maquinaTextGameObject.SetActive(false);
            playerText.text = _text;
            playerTextGameObject.GetComponent<Image>().color = Color.red;
            playerTextGameObject.SetActive(true);
        } else {
            playerTextGameObject.SetActive(false);
            maquinaText.text = _text;
            maquinaTextGameObject.GetComponent<Image>().color = Color.red;
            maquinaTextGameObject.SetActive(true);
        }
    }

    //Ponemos la respuesta para que el jugador pueda elegir
    public IEnumerator PonerRespuestaCoroutine(float _time)
    {
        for (int i = 0; i < DataBase.instance.respuestas.Count; i++) if (respuestasInsultosInstances.Count > i) respuestasInsultosInstances[i].SetActive(false);

        yield return new WaitForSeconds(_time);

        for (int i = 0; i < DataBase.instance.respuestas.Count; i++) {
            if (respuestasInsultosInstances.Count < i) {
                GameObject riObj = Instantiate(respuetasInsultoPrefab);
                riObj.GetComponent<RespuestasOInsultos>().Create(DataBase.instance.respuestas[i], true, i, this);
                riObj.GetComponent<Image>().color = (Color.green);
                riObj.transform.SetParent(contentRespuesta.transform);
                respuestasInsultosInstances.Add(riObj);
            } else {
                respuestasInsultosInstances[i].GetComponent<RespuestasOInsultos>().Create(DataBase.instance.respuestas[i], true, i, this);
                respuestasInsultosInstances[i].GetComponent<Image>().color = (Color.green);
                respuestasInsultosInstances[i].SetActive(true);
            }
        }
        if (turnoJugador) JugarMaquina();

    }

    //Comprobamos los resultado si ha finalizado o no y si quien ha ganado la conversa
    public void Resultado(string _text, bool _acertado)
    {
        if (turnoJugador) {
            if (_acertado) {
                vidasPokeballsPlayer[puntuacionMaquina].gameObject.SetActive(false);
                puntuacionMaquina++;
                fallaAudio.Play();
                puntuacionMaquinaText.text = puntuacionMaquina.ToString();
                switch (puntuacionMaquina) {
                    case 0:
                        pokemonImagePlayer.sprite = DataBase.instance.pokemons[PlayerPrefs.GetInt(SaveGame.pokemon1)];
                        break;
                    case 1:
                        pokemonImagePlayer.sprite = DataBase.instance.pokemons[PlayerPrefs.GetInt(SaveGame.pokemon2)];
                        break;
                    case 2:
                        pokemonImagePlayer.sprite = DataBase.instance.pokemons[PlayerPrefs.GetInt(SaveGame.pokemon3)];
                        break;
                }
            } else {
                vidasPokeballsMaquina[puntuacionPlayer].gameObject.SetActive(false);
                puntuacionPlayer++;
                aciertaAudio.Play();
                puntuacionPlayerText.text = puntuacionPlayer.ToString();
                pokemonImageMaquina.sprite = DataBase.instance.pokemons[Random.Range(0, DataBase.instance.pokemons.Length)];

            }
            playerTextGameObject.SetActive(false);
            maquinaText.text = _text;
            maquinaTextGameObject.GetComponent<Image>().color = Color.green;

            maquinaTextGameObject.SetActive(true);

        } else {
            if (_acertado) {
                vidasPokeballsMaquina[puntuacionPlayer].gameObject.SetActive(false);
                puntuacionPlayer++;
                aciertaAudio.Play();
                puntuacionPlayerText.text = puntuacionPlayer.ToString();
                pokemonImageMaquina.sprite = DataBase.instance.pokemons[Random.Range(0, DataBase.instance.pokemons.Length)];

            } else {
                vidasPokeballsPlayer[puntuacionMaquina].gameObject.SetActive(false);
                puntuacionMaquina++;
                fallaAudio.Play();
                puntuacionMaquinaText.text = puntuacionMaquina.ToString();
                switch (puntuacionMaquina) {
                    case 0:
                        pokemonImagePlayer.sprite = DataBase.instance.pokemons[PlayerPrefs.GetInt(SaveGame.pokemon1)];
                        break;
                    case 1:
                        pokemonImagePlayer.sprite = DataBase.instance.pokemons[PlayerPrefs.GetInt(SaveGame.pokemon2)];
                        break;
                    case 2:
                        pokemonImagePlayer.sprite = DataBase.instance.pokemons[PlayerPrefs.GetInt(SaveGame.pokemon3)];
                        break;
                }

            }
            maquinaTextGameObject.SetActive(false);
            playerText.text = _text;
            playerTextGameObject.GetComponent<Image>().color = Color.green;
            playerTextGameObject.SetActive(true);
        }

        turnoJugador = !turnoJugador;
    }

    //Comprobamos sin se ha terminado la partida
    private bool Finalizado()
    {
        bool gameOver = false;
        if (puntuacionMaquina >= 3 || puntuacionPlayer >= 3) {
            gameOver = true;
        }
        return gameOver;
    }


    private void JugarMaquina()
    {
        respuestasInsultosInstances[Random.Range(0, DataBase.instance.insultos.Count)].GetComponent<RespuestasOInsultos>().Click();
    }

    private void TextoPlayer(string _text)
    {
        playerText.text = _text;
        maquinaTextGameObject.SetActive(false);
        playerTextGameObject.SetActive(true);
    }

    private void Textomaquina(string _text)
    {
        maquinaText.text = _text;
        playerTextGameObject.SetActive(false);
        maquinaTextGameObject.SetActive(true);
    }
    private void GameOver()
    {
        SceneManager.LoadScene(2);

    }

}
