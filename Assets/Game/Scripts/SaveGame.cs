﻿using UnityEngine;
using UnityEngine;

public class SaveGame 
{
    //Lo nombres de los saves que guardaremos, lo cogeremos desde aquí para evitar futuros fallos 
    //por si lo escribimos mal o por si se añade alguna repetido
    public static string idioma = "Idioma";
    public static string entrenador = "Entrenador";
    public static string pokemon1 = "Pokemon1";
    public static string pokemon2 = "Pokemon2";
    public static string pokemon3 = "Pokemon3";

    //Para guardar datos llamaremos a estos metodos para que en un futuro sea mas fácil encontrar posibles fallos
    //Guardamos el idioma para la próxima partida
    public static void Idioma(int _idioma)
    {
        PlayerPrefs.SetInt(idioma, _idioma);
    }

    //Guardamos el entrenador que ha selecionado para la proxima partida
    public static void Entrenador(int _idioma)
    {
        PlayerPrefs.SetInt(entrenador, _idioma);
    }

    //Guardamos el pokemon 1 que ha selecionado para la proxima partida
    public static void Pokemon1(int _idioma)
    {
        PlayerPrefs.SetInt(pokemon1, _idioma);
    }

    //Guardamos el pokemon 2 que ha selecionado para la proxima partida
    public static void Pokemon2(int _idioma)
    {
        PlayerPrefs.SetInt(pokemon2, _idioma);
    }

    //Guardamos el pokemon 3 que ha selecionado para la proxima partida
    public static void Pokemon3(int _idioma)
    {
        PlayerPrefs.SetInt(pokemon3, _idioma);
    }
}
