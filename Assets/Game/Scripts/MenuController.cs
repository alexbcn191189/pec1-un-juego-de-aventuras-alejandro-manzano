﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Controlara las acciones del menu
 */

public class MenuController : MonoBehaviour {

    //Los textos de los botones
    public Text jugarText, salirText, idiomaText, idioma2Text;

    //la posicion de la id de las imagenes de pokemons y entrenadores y sus imagenes
    public int EntrenadorPos, pokemon1Pos, pokemon2Pos, pokemon3Pos;
    public Image entrenador, pokemon1, pokemon2, pokemon3;

    //El sonido del menu principal
    public AudioSource audioHome;

    void Start()
    {
        //Añadimos el sonido aleatoriamente
        audioHome.clip = DataBase.instance.sounds[Random.Range(0, DataBase.instance.sounds.Length)];
        audioHome.Play();

        //Comprobamos que no se haya jugado previamente y si se ha guardado algun dato
        if (PlayerPrefs.HasKey(SaveGame.entrenador)) {
            EntrenadorPos = PlayerPrefs.GetInt(SaveGame.entrenador);
            pokemon1Pos = PlayerPrefs.GetInt(SaveGame.pokemon1);
            pokemon2Pos = PlayerPrefs.GetInt(SaveGame.pokemon2);
            pokemon3Pos = PlayerPrefs.GetInt(SaveGame.pokemon3);
            entrenador.sprite = DataBase.instance.entrenadores[EntrenadorPos];
            pokemon1.sprite = DataBase.instance.pokemons[pokemon1Pos];
            pokemon2.sprite = DataBase.instance.pokemons[pokemon2Pos];
            pokemon3.sprite = DataBase.instance.pokemons[pokemon3Pos];
        } else {
            pokemon2Pos = 3;
            pokemon3Pos = 6;
        }

        //Ponemos el idioma del juego si ya se ha jugado previamente, si no, dejamos el default
        if (PlayerPrefs.HasKey(SaveGame.idioma)) {
            if (PlayerPrefs.GetInt(SaveGame.idioma) == 0) {
                ButtonCambiarIdiomaTemps(0, "Español", "Jugar", "Salir", DataBase.instance.letraNormal);
            } else if (PlayerPrefs.GetInt(SaveGame.idioma) == 1) {
                ButtonCambiarIdiomaTemps(1, "Unown", "Jugar", "Salir", DataBase.instance.letraUnow);
            } else {
                ButtonCambiarIdiomaTemps(2, "Ingles", "Play", "Exit", DataBase.instance.letraNormal);
            }
            ReadTXT.instance.Start();
        }
    }

    //Se mostrara todos los botones del menu
    #region Buttons
    public void ButtonJugar()
    {
        SaveGame.Entrenador(EntrenadorPos);
        SaveGame.Pokemon1(pokemon1Pos);
        SaveGame.Pokemon2(pokemon2Pos);
        SaveGame.Pokemon3(pokemon3Pos);
        SceneManager.LoadScene("Main");
    }

    public void ButtonSalir()
    {
        Application.Quit();
    }

    //Cambiamos de idioma 0 o null es español, 1 es ingles
    public void ButtonCambiarIdioma()
    {
        if (PlayerPrefs.HasKey(SaveGame.idioma)) {
            if (PlayerPrefs.GetInt(SaveGame.idioma) == 0) {
                ButtonCambiarIdiomaTemps(1, "Unown", "Jugar", "Salir", DataBase.instance.letraUnow);
            } else if (PlayerPrefs.GetInt(SaveGame.idioma) == 1) {
                ButtonCambiarIdiomaTemps(2, "Ingles", "Play", "Exit", DataBase.instance.letraNormal);
            } else {
                ButtonCambiarIdiomaTemps(0, "Español", "Jugar", "Salir", DataBase.instance.letraNormal);
            }
        } else {
            ButtonCambiarIdiomaTemps(2, "Ingles", "Play", "Exit", DataBase.instance.letraNormal);
        }
        ReadTXT.instance.Start();
    }

    public void ButtonCambiarIdiomaTemps(int _idiomaSave, string _idiomaText, string _jugarText, string _salirText, Font _font)
    {
        SaveGame.Idioma(_idiomaSave);
        idiomaText.text = _idiomaText;
        jugarText.text = _jugarText;
        salirText.text = _salirText;
        salirText.font = jugarText.font = idiomaText.font = idioma2Text.font = _font;

    }

    public void ButtonLeftCambiarPokemon(int _num)
    {
        ButtonCambiarPokemon(_num, -1);
    }

    public void ButtonRigthCambiarPokemon(int _num)
    {
        ButtonCambiarPokemon(_num, 1);
    }

    public void ButtonCambiarPokemon(int _num, int _direccion)
    {
        switch (_num) {
            case 0:
            EntrenadorPos = CambiarPokemon(EntrenadorPos, _direccion, DataBase.instance.entrenadores, entrenador);
            break;
            case 1:
            pokemon1Pos = CambiarPokemon(pokemon1Pos, _direccion, DataBase.instance.pokemons, pokemon1);
            break;
            case 2:
            pokemon2Pos = CambiarPokemon(pokemon2Pos, _direccion, DataBase.instance.pokemons, pokemon2);
            break;
            case 3:
            pokemon3Pos = CambiarPokemon(pokemon3Pos, _direccion, DataBase.instance.pokemons, pokemon3);
            break;
        }
    }
    #endregion

    private int CambiarPokemon(int _iterador, int _direccion, Sprite[] _sprites, Image _entrenador)
    {
        _iterador += _direccion;
        if (_iterador < 0) {
            _iterador = _sprites.Length - 1;
        } else if (_iterador >= _sprites.Length) {
            _iterador = 0;
        }
        _entrenador.sprite = _sprites[_iterador];
        return _iterador;
    }
}
